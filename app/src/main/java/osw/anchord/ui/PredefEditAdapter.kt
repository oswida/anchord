package osw.anchord.ui

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class PredefEditAdapter(context: Context) : ArrayAdapter<String>(context, android.R.layout.simple_list_item_1) {

    val items = mapOf(
            "Chord section []" to "[]",
            "Directive section {}" to "{}",
            "Song title" to "{title:}", "Comment" to "{c:}",
            "Chord image" to "{chord:}", "Chorus" to "{soc: }\n\n{eoc}\n")
    val offsets = mapOf("Chord section []" to 1, "Song title" to 7,
            "Directive section {}" to 1, "Comment" to 3, "Chord image" to 7, "Chorus" to 8)

    override fun getCount(): Int {
        return items.keys.size
    }

    fun itemAt(position: Int): String {
        val key = items.keys.toList().sorted()[position]
        return items[key]!!
    }

    fun keyAt(position: Int): String {
        return items.keys.toList().sorted()[position]
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val item = items.keys.toList().sorted()[position]
        var view = convertView
        if (view == null)
            view = View.inflate(context, android.R.layout.simple_list_item_1, null)
        val txt = view!!.findViewById<TextView>(android.R.id.text1)
        txt.text = item
        txt.tag = items[item]
        return view!!
    }
}