package osw.anchord.ui

import android.content.Context
import android.util.AttributeSet

class EditTextPreference(context: Context, attrs: AttributeSet) : android.support.v7.preference.EditTextPreference(context, attrs) {

    override fun setText(text: String) {
        super.setText(text)
        notifyChanged()
    }

    override fun getSummary(): CharSequence {
        val text = super.getText()
        val summary = super.getSummary().toString()
        return String.format(summary, text ?: "")
    }
}