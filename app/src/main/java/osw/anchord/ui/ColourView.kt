package osw.anchord.ui

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import osw.anchord.R
import osw.anchord.db.entities.Song
import osw.anchord.util.ColorHelper
import osw.anchord.util.DbHelper


class ColourView(context: Context?, val song: Song) : LinearLayout(context) {
    var dialog: DialogInterface? = null

    init {
        val inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v = inflater.inflate(R.layout.colours, null)
        addView(v)
        gravity = Gravity.CENTER
        background = ColorDrawable(Color.WHITE)
        var iv = v.findViewById<ImageView>(R.id.colour_item_red)
        iv.setColorFilter(ColorHelper.red(context))
        iv.setOnClickListener {
            setColour("red")
        }
        iv = v.findViewById<ImageView>(R.id.colour_item_black)
        iv.setColorFilter(ColorHelper.black(context))
        iv.setOnClickListener {
            setColour("black")
        }
        iv = v.findViewById<ImageView>(R.id.colour_item_blue)
        iv.setColorFilter(ColorHelper.blue(context))
        iv.setOnClickListener {
            setColour("blue")
        }
        iv = v.findViewById<ImageView>(R.id.colour_item_empty)
        iv.setOnClickListener {
            setColour("")
        }
        iv = v.findViewById<ImageView>(R.id.colour_item_green)
        iv.setColorFilter(ColorHelper.green(context))
        iv.setOnClickListener {
            setColour("green")
        }
        iv = v.findViewById<ImageView>(R.id.colour_item_yellow)
        iv.setColorFilter(ColorHelper.yellow(context))
        iv.setOnClickListener {
            setColour("yellow")
        }
        iv = v.findViewById<ImageView>(R.id.colour_item_purple)
        iv.setColorFilter(ColorHelper.magenta(context))
        iv.setOnClickListener {
            setColour("magenta")
        }
        v.findViewById<ImageView>(R.id.colour_item_cancel).setOnClickListener {
            dialog!!.dismiss()
        }
    }

    private fun setColour(clr: String) {
        song.color = clr
        DbHelper.postDbTask(context) { db ->
            db.songDao().save(song)
        }
        dialog!!.dismiss()
    }
}