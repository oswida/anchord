package osw.anchord.ui

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import osw.anchord.R
import osw.anchord.util.AppSettings
import osw.anchord.util.ColorHelper

class ColourFilterView(context: Context?) : LinearLayout(context) {

    var dialog: DialogInterface? = null
    val selected = mutableMapOf<String, Boolean>("red" to false, "black" to false,
            "blue" to false, "green" to false, "yellow" to false, "magenta" to false)
    val resources = mutableMapOf<String, Int>("red" to R.id.colour_item_red, "black" to R.id.colour_item_black,
            "blue" to R.id.colour_item_blue, "green" to R.id.colour_item_green,
            "yellow" to R.id.colour_item_yellow, "magenta" to R.id.colour_item_purple)
    val selcol = mapOf<Boolean, Int>(true to Color.parseColor("grey"), false to Color.parseColor("white"))
    val colmap = mapOf("red" to R.color.catRed, "blue" to R.color.catBlue, "green" to R.color.catGreen,
            "yellow" to R.color.catYellow, "magenta" to R.color.catMagenta, "black" to R.color.catBlack)


    init {
        val inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v = inflater.inflate(R.layout.colours_select, null)
        addView(v)
        gravity = Gravity.CENTER
        background = ColorDrawable(Color.WHITE)

        resources.keys.forEach { color ->
            val iv = v.findViewById<ImageView>(resources[color]!!)
            iv.setColorFilter(ColorHelper.color(context, colmap[color]!!))

            if (AppSettings.selectedStars.contains(color)) {
                iv.setBackgroundColor(selcol[true]!!)
                selected[color] = true
            } else {
                iv.setBackgroundColor(selcol[false]!!)
                selected[color] = false
            }
            iv.setOnClickListener {
                selected[color] = !selected[color]!!
                iv.setBackgroundColor(selcol[selected[color]]!!)
            }
        }
        v.findViewById<ImageView>(R.id.colour_item_selectall).setOnClickListener {
            selected.keys.forEach {
                selected[it] = false
                val iv = v.findViewById<ImageView>(resources[it]!!)
                iv.setBackgroundColor(selcol[selected[it]]!!)
            }
        }
        v.findViewById<ImageView>(R.id.colour_item_cancel).setOnClickListener {
            dialog!!.dismiss()
        }
        v.findViewById<ImageView>(R.id.colour_item_accept).setOnClickListener {
            dialog!!.dismiss()
        }
    }
}