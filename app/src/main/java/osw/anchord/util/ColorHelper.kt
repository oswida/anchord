package osw.anchord.util

import android.content.Context
import android.support.v4.content.res.ResourcesCompat
import osw.anchord.R


object ColorHelper {

    fun color(context: Context, res: Int): Int {
        return ResourcesCompat.getColor(context.resources, res, null)
    }

    fun yellow(context: Context): Int {
        return ResourcesCompat.getColor(context.resources, R.color.catYellow, null)
    }

    fun red(context: Context): Int {
        return ResourcesCompat.getColor(context.resources, R.color.catRed, null)
    }

    fun blue(context: Context): Int {
        return ResourcesCompat.getColor(context.resources, R.color.catBlue, null)
    }

    fun green(context: Context): Int {
        return ResourcesCompat.getColor(context.resources, R.color.catGreen, null)
    }

    fun magenta(context: Context): Int {
        return ResourcesCompat.getColor(context.resources, R.color.catMagenta, null)
    }

    fun black(context: Context): Int {
        return ResourcesCompat.getColor(context.resources, R.color.catBlack, null)
    }
}