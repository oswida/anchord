package osw.anchord.util

import android.content.ContentResolver
import android.content.Context
import android.content.res.AssetManager
import android.net.Uri
import android.os.Environment
import java.io.*

/**
 * File operations utility class.
 */
object FileHelper {

    /**
     * Read text from a provided Uri.
     */
    @Throws(IOException::class)
    fun readTextFromUri(contentResolver: ContentResolver, uri: Uri): String {
        val inputStream = contentResolver.openInputStream(uri)
        val reader = BufferedReader(InputStreamReader(
                inputStream))
        val stringBuilder = StringBuilder()
        var line = reader.readLine()
        while (line != null) {
            stringBuilder.append(line).append("\n")
            line = reader.readLine()
        }
        return stringBuilder.toString()
    }

    @Throws(IOException::class)
    fun saveSongToAppDir(context: Context, filename: String, content: String) {
        val file = File(context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), filename)
        val outputStream = FileOutputStream(file)
        val writer = BufferedWriter(OutputStreamWriter(outputStream))
        writer.write(content)
        writer.close()
    }

    /**
     * Read text from file in asset directory.
     */
    @Throws(IOException::class)
    fun readTextFromAsset(assetManager: AssetManager, path: String): String {
        val inputStream = assetManager.open(path)
        val reader = BufferedReader(InputStreamReader(
                inputStream))
        val stringBuilder = StringBuilder()
        var line = reader.readLine()
        while (line != null) {
            stringBuilder.append(line).append("\n")
            line = reader.readLine()
        }
        return stringBuilder.toString()
    }

    fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

}