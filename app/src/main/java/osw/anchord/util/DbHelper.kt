package osw.anchord.util

import android.content.Context
import osw.anchord.db.AppDatabase
import osw.anchord.db.DbWorker

object DbHelper {

    fun postDbTask(context: Context, func: (AppDatabase) -> Unit) {
        DbWorker.getInstance()!!.postTask(Runnable {
            func(AppDatabase.getInstance(context)!!)
        })
    }

}