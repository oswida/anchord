package osw.anchord.util
import android.app.AlertDialog
import android.content.Context
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView

object Dialogs {

    fun info(context: Context?, title: String, message: String) {
        val builder = AlertDialog.Builder(context)
        with(builder) {
            setTitle(title)
            setMessage(message)
            create().show()
        }
    }

    fun inputString(context: Context?, title: String, defaultVal: String, func: (text: String?) -> Unit) {
        val builder = AlertDialog.Builder(context)
        with(builder) {
            setTitle(title)
            val edit = EditText(context)
            edit.setText(defaultVal)
            edit.setPadding(15, 15, 15, 15)
            setView(edit)
            setNegativeButton("Cancel") { dialog, which ->
                dialog.dismiss()
            }
            setPositiveButton("Ok") { dialog, which ->
                dialog.dismiss()
                func(edit.text.toString())
            }
            create().show()
        }
    }

    fun confirmation(context: Context?, title: String, message: String, func: () -> Unit) {
        val builder = AlertDialog.Builder(context)
        with(builder) {
            setTitle(title)
            setMessage(message)
            setPositiveButton("Ok") { dialog, which ->
                dialog.dismiss()
                func()
            }
            setNegativeButton("Cancel") { dialog, which ->
                dialog.dismiss()
            }
            create().show()
        }
    }

    fun inputItemWithDesc(context: Context?, title: String, nameTitle: String,
                          descTitle: String, namedefVal: String, descdefVal: String,
                          func: (String, String) -> Unit) {
        val builder = AlertDialog.Builder(context)
        with(builder) {
            setTitle(title)
            val layout = LinearLayout(context)
            layout.setPadding(15, 15, 15, 15)
            layout.orientation = LinearLayout.VERTICAL
            val titleText = TextView(context)
            titleText.text = nameTitle
            val descText = TextView(context)
            descText.text = descTitle
            val nameEdit = EditText(context)
            nameEdit.setText(namedefVal)
            val descEdit = EditText(context)
            descEdit.setText(descdefVal)
            layout.addView(titleText)
            layout.addView(nameEdit)
            layout.addView(descText)
            layout.addView(descEdit)
            setView(layout)
            setPositiveButton("Ok") { dialog, which ->
                dialog.dismiss()
                func(nameEdit.text.toString(), descEdit.text.toString())
            }
            setNegativeButton("Cancel") { dialog, which ->
                dialog.dismiss()
            }
            create().show()
        }
    }

    fun inputItemWithDescAndTags(context: Context?, title: String, nameTitle: String,
                          descTitle: String, namedefVal: String, descdefVal: String,
                          tagsDefVal:String, func: (String, String, String) -> Unit) {
        val builder = AlertDialog.Builder(context)
        with(builder) {
            setTitle(title)
            val layout = LinearLayout(context)
            layout.setPadding(15, 15, 15, 15)
            layout.orientation = LinearLayout.VERTICAL
            val titleText = TextView(context)
            titleText.text = nameTitle
            val descText = TextView(context)
            descText.text = descTitle
            val nameEdit = EditText(context)
            nameEdit.setText(namedefVal)
            val descEdit = EditText(context)
            descEdit.setText(descdefVal)
            val tagsEdit = EditText(context)
            tagsEdit.setText(tagsDefVal)
            layout.addView(titleText)
            layout.addView(nameEdit)
            layout.addView(descText)
            layout.addView(descEdit)
            val tt = TextView(context)
            tt.text = "Tags"
            layout.addView(tt)
            layout.addView(tagsEdit)
            setView(layout)
            setPositiveButton("Ok") { dialog, which ->
                dialog.dismiss()
                func(nameEdit.text.toString(), descEdit.text.toString(), tagsEdit.text.toString())
            }
            setNegativeButton("Cancel") { dialog, which ->
                dialog.dismiss()
            }
            create().show()
        }
    }

    fun selectOption(context: Context?, title: String, items: List<String>, func: (String) -> Unit) {
        val builder = AlertDialog.Builder(context)
        with(builder) {
            setTitle(title)
            setItems(items.toTypedArray()) { dialog, which ->
                dialog.dismiss()
                func(items[which])
            }
            create().show()
        }
    }

    fun selectSingleOption(context: Context?, title: String, items: List<String>,
                           selected: Int, clearButton: Boolean = false, func: (String) -> Unit) {
        val builder = AlertDialog.Builder(context)
        with(builder) {
            setTitle(title)
            setSingleChoiceItems(items.toTypedArray(), selected) { dialog, which ->
                dialog.dismiss()
                func(items[which])
            }
            if (clearButton) {
                setPositiveButton("Clear") {dialog, which ->
                    dialog.dismiss()
                    func("")
                }
            }
            create().show()
        }
    }

    fun selectMultiple(context: Context?, title: String, items: List<String>,
                       checked: BooleanArray, func: (List<String>) -> Unit) {
        val builder = AlertDialog.Builder(context)
        with(builder) {
            setTitle(title)
            setMultiChoiceItems(items.toTypedArray(), checked) { dialog, which, isChecked ->
                checked[which] = isChecked
            }
            setPositiveButton("Select") {dialog, which ->
                dialog.dismiss()
                val lst = mutableListOf<String>()
                for (i in 0 until checked.size) {
                    if (checked[i]) {
                        lst.add(items[i])
                    }
                }
                func(lst)
            }
            setNegativeButton("Cancel") { dialog, which ->
                dialog.dismiss()
            }
            create().show()
        }
    }


}