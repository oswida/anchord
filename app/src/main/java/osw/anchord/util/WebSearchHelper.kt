package osw.anchord.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import org.jsoup.Jsoup
import osw.anchord.fragments.websearch.WebItem


object WebSearchHelper {

    private const val BASE_URI = "http://www.tekstowo.pl"

    fun findTitle(title: String): List<WebItem> {
        val result = mutableListOf<WebItem>()
        val doc = Jsoup.connect("$BASE_URI/szukaj,wykonawca,,tytul,$title")
                .maxBodySize(0)
                .ignoreContentType(true)
                .ignoreHttpErrors(true)
                .followRedirects(true)
                .get()

        val start = doc.selectFirst("html body div#contener div#center div.right-column")
        val elems = start.child(1).select("div.box-przeboje a")
        for (el in elems) {
            result.add(WebItem(el.wholeText(), el.attr("href")))
        }
        return result
    }

    fun extractSong(url: String): String {
        val doc = Jsoup.connect("${WebSearchHelper.BASE_URI}$url")
                .maxBodySize(0)
                .ignoreContentType(true)
                .ignoreHttpErrors(true)
                .followRedirects(true)
                .get()
        val el = doc.selectFirst("html body div#contener div#center div.right-column div.tekst div.song-text")
        val repregex = Regex("<a.*/a>|<div[^/]*/div>|<p>(&nbsp;)*</p>|<h2[^/]*/h2>|<br>")
        var txt = el.html()
        txt = repregex.replace(txt, "")
        return txt
    }

    fun hasNetwork(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return (activeNetwork?.isConnectedOrConnecting == true)
    }

}