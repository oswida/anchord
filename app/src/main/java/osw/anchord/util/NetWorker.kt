package osw.anchord.util

import android.arch.persistence.room.Room
import android.content.Context
import android.os.Handler
import android.os.HandlerThread
import osw.anchord.db.AppDatabase

class NetWorker(name: String) : HandlerThread(name) {

    private lateinit var mWorkerHandler: Handler

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        mWorkerHandler = Handler(looper)
    }

    fun postTask(task: Runnable) {
        mWorkerHandler.post(task)
    }

    companion object {
        var INSTANCE: NetWorker? = null

        fun getInstance(): NetWorker? {
            if (INSTANCE == null) {
                synchronized(NetWorker::class) {
                    INSTANCE = NetWorker("NETWORKER")
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }
}
