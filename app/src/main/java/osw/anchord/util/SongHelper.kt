package osw.anchord.util

import android.content.Context
import android.support.v7.preference.PreferenceManager
import osw.anchord.db.entities.Song


/**
 * Type of the song element.
 */
enum class SongElementType {
    TITLE,
    SUBTITLE,
    COMMENT,
    CHORUS,
    VERSE,
    TEXT,
    META,
    CHORD
}

/**
 * Song element.
 */
data class SongElement(val type: SongElementType = SongElementType.TEXT,
                       val title: String = "",
                       val content: String = "",
                       val original: String = "",
                       val range: IntRange)

/**
 * Guitar chord element.
 */
data class ChordElement(var pos: Int = 0, val content: String = "", val range: IntRange)

/**
 * Song analysis utility class (static).
 */
object SongHelper {

    private var styleInvisible: String = "visibility:hidden"
    private var titleTpl: String = "font-size:%.2fem;font-weight:bold;margin-top:0.3em;margin-bottom:0.3em;"
    private var styleChorus: String = "margin-left: 1em; margin-bottom: 0.5em;margin-top:0.5em"
    private var chordTpl: String = "font-style:%s;font-size:%.2fem;font-weight:%s;color:%s"
    private var metaTpl: String = "font-style:%s;font-size:%.2fem;color:%s"
    private var commentTpl: String = "font-style:italic;font-size:%fem"
    val chordImages: Map<String, String> = mapOf(
            "A" to "A", "A6" to "A6", "A7" to "A7", "A9" to "A9", "A+" to "Aplus", "Am6" to "Am6",
            "a6" to "Am6", "Am7" to "Am7", "a7" to "Am7", "a" to "a", "Am" to "Am",
            "B" to "B", "B6" to "B6", "B7" to "B7", "B9" to "B9", "B+" to "Bplus", "Bm6" to "Bm6",
            "b6" to "b6", "Bm7" to "Bm7", "b7" to "b7", "b" to "b", "b" to "Bm",
            "C" to "C", "C6" to "C6", "C7" to "C7", "C9" to "C9", "C+" to "Cplus", "Cm6" to "Cm6",
            "c6" to "c6", "Cm7" to "Cm7", "c7" to "c7", "c" to "c", "Cm" to "Cm",
            "D" to "D", "D6" to "D6", "D7" to "D7", "D9" to "D9", "D+" to "Dplus", "Dm6" to "Dm6",
            "d6" to "d6", "Dm7" to "Dm7", "d7" to "d7", "d" to "d", "Dm" to "Dm",
            "E" to "E", "E6" to "E6", "E7" to "E7", "E9" to "E9", "E+" to "Eplus", "Em6" to "Em6",
            "e6" to "e6", "Em7" to "Em7", "e7" to "e7", "e" to "e", "Em" to "Em",
            "F" to "F", "F6" to "F6", "F7" to "F7", "F9" to "F9", "F+" to "Fplus", "Fm6" to "Fm6",
            "f6" to "f6", "Fm7" to "Fm7", "f7" to "f7", "f" to "f", "Fm" to "Fm",
            "G" to "G", "G6" to "G6", "G7" to "G7", "G9" to "G9", "G+" to "Gplus", "Gm6" to "Gm6",
            "g6" to "g6", "Gm7" to "Gm7", "g7" to "g7", "g" to "g", "Gm" to "Gm",
            "H" to "H", "H6" to "H6", "H7" to "H7", "H9" to "H9", "H+" to "Hplus", "Hm6" to "Hm6",
            "h6" to "h6", "Hm7" to "Hm7", "h7" to "h7", "h" to "h", "Hm" to "Hm",
            "F#" to "Fhash", "F#m" to "Fmhash", "f#" to "fhash")
    var emptySongContent = "{title: %s}\n\n"
    val foundChords = mutableListOf<String>()

    private val TITLE_REGEX = "\\{(?:title|t):\\s*([^\\}\\{]*)\\s*\\}\\s*\\n"
    private val SUBTITLE_REGEX = "\\{(?:subtitle|st):\\s*([^\\}\\{]*)\\s*\\}\\s*\\n"
    private val COMMENT_REGEX = "\\{(?:comment|c):([^\\}\\{]*)\\}\\s*\\n"
    private val CHORD_REGEX = "\\{chord:\\s*([a-zA-Z][a-zA-Z0-9+#]*)\\s*\\}\\s*\\n"
    private val LC_REGEX = "(?m)^#.*\\n?"
    private val CHORUS_REGEX = "\\{(?:start_of_chorus|soc)(\\:[^\\}\\{]*)?\\s*\\}\\s*\\n([^\\}\\{]*)\\{(?:end_of_chorus|eoc)\\s*\\}\\s*\\n"
    private val VERSE_REGEX = "\\{(?:start_of_verse|sov)(\\:[^\\}\\{]*)?\\s*\\}\\s*\\n([^\\}\\{]*)\\{(?:end_of_verse|eov)\\s*\\}\\s*\\n"
    private val KEY_REGEX = "\\{key:\\s*([a-zA-Z])\\s*\\}\\s*\\n"
    private val ARTIST_REGEX = "\\{artist:\\s*([^\\}\\{]*)\\s*\\}\\s*\\n"
    private val COMPOSER_REGEX = "\\{composer:\\s*([^\\}\\{]*)\\s*\\}\\s*\\n"
    private val ALBUM_REGEX = "\\{album:\\s*([^\\}\\{]*)\\s*\\}\\s*\\n"
    private val LYRICIST_REGEX = "\\{lyricist:\\s*([^\\{\\}]*)\\s*\\}\\s*\\n"
    private val TIME_REGEX = "\\{time:\\s*([0-9]+(?:\\/[0-9]+)?)\\s*\\}\\s*\\n"
    private val TEMPO_REGEX = "\\{tempo:\\s*([0-9]+)\\s*\\}\\s*\\n"
    private val DURATION_REGEX = "\\{duration:\\s*([0-9]+(?:\\:[0-9]+)?)\\s*\\}\\s*\\n"

    val CHORD_NAMES = listOf("C", "D", "E", "F", "G", "A", "B", "H")

    /**
     * Create CSS style section for output HTML.
     */
    private fun createStyles(context: Context): String {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context.applicationContext)
        val sb = StringBuffer()
        with(sharedPref) {
            val basicSize = getString("pref_key_text_size", "14").toInt()
            val titleM = getString("pref_key_title_size", "1.5").toFloat()
            val chordM = getString("pref_key_chord_size", "0.9").toFloat()
            val commentM = getString("pref_key_comment_size", "0.7").toFloat()
            val metaM = getString("pref_key_meta_size", "0.7").toFloat()

            sb.append("<style>")
            sb.append(".verse { margin-top:0.5em;margin-bottom:0.5em; }")
            sb.append(".invisible {").append(styleInvisible).append("} ")
            sb.append(".title {").append(titleTpl.format(java.util.Locale.US, titleM)).append("} ")
            sb.append(".subtitle {").append(titleTpl.format(java.util.Locale.US, titleM * 0.7)).append("} ")
            sb.append(".comment {").append(commentTpl.format(java.util.Locale.US, commentM)).append("} ")
            sb.append(".chorus {").append(styleChorus).append("} ")
            sb.append(".chord {").append(chordTpl.format(java.util.Locale.US,
                    "normal", chordM,
                    "bold", getString("pref_key_chord_color", "orange"))).append("} ")
            sb.append(".metadata {").append(metaTpl.format(java.util.Locale.US,
                    "italic", metaM,
                    getString("pref_key_meta_color", "orange"))).append("} ")
            sb.append("body { font-size:%spt; }".format(java.util.Locale.US, basicSize))
            sb.append("img { }")
            sb.append(" .chordline { }")
            sb.append("</style>")
        }
        return sb.toString()
    }

    /**
     * Import song from the text file.
     */
    fun importSong(text: String): Song {
        val song = Song()
        song.content = text
        val reg = Regex(TITLE_REGEX, RegexOption.IGNORE_CASE)
        val matchResult = reg.find(text)
        if (matchResult !== null) {
            val (tt) = matchResult.destructured
            song.title = tt.trim()
        } else {
            song.title = "Unknown"
        }
        return song
    }


    /**
     * Parse song text and create a list of SongElement objects.
     */
    fun parseSong(content: String): List<SongElement> {
        val elements = mutableListOf<SongElement>()

        val titleReg = Regex(TITLE_REGEX, RegexOption.IGNORE_CASE)
        val subtitleReg = Regex(SUBTITLE_REGEX, RegexOption.IGNORE_CASE)
        val commentReg = Regex(COMMENT_REGEX, RegexOption.IGNORE_CASE)
        val chordReg = Regex(CHORD_REGEX)
        val lcommentReg = Regex(LC_REGEX)
        val chorusReg = Regex(CHORUS_REGEX, RegexOption.IGNORE_CASE)
        val verseReg = Regex(VERSE_REGEX, RegexOption.IGNORE_CASE)

        val keyReg = Regex(KEY_REGEX, RegexOption.IGNORE_CASE)
        val artistReg = Regex(ARTIST_REGEX, RegexOption.IGNORE_CASE)
        val composerReg = Regex(COMPOSER_REGEX, RegexOption.IGNORE_CASE)
        val albumReg = Regex(ALBUM_REGEX, RegexOption.IGNORE_CASE)
        val lyricistReg = Regex(LYRICIST_REGEX, RegexOption.IGNORE_CASE)
        val timeReg = Regex(TIME_REGEX, RegexOption.IGNORE_CASE)
        val tempoReg = Regex(TEMPO_REGEX, RegexOption.IGNORE_CASE)
        val durationReg = Regex(DURATION_REGEX, RegexOption.IGNORE_CASE)

        var fres = titleReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.TITLE, it.groups[1]!!.value,
                    it.groups[1]!!.value, it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = subtitleReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.SUBTITLE, it.groups[1]!!.value,
                    it.groups[1]!!.value, it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = commentReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.COMMENT, it.groups[1]!!.value,
                    it.groups[1]!!.value, it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = lcommentReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.TEXT, "", "",
                    it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = chorusReg.findAll(content)
        fres.forEach {
            var valct = it.groups[1]?.value?.substring(1)
            val valcc = it.groups[2]?.value?.trim()
            if (valct === null) valct = ""
            val el = SongElement(SongElementType.CHORUS, valct, valcc!!,
                    it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = chordReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.CHORD, it.groups[1]!!.value,
                    it.groups[1]!!.value, it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = verseReg.findAll(content)
        fres.forEach {
            var valvt = it.groups[1]?.value?.substring(1)
            val valvc = it.groups[2]?.value?.trim()
            if (valvt === null) valvt = ""
            val el = SongElement(SongElementType.VERSE, valvt, valvc!!,
                    it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = keyReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.META, "Key",
                    it.groups[1]!!.value, it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = artistReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.META, "Artist",
                    it.groups[1]!!.value, it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = composerReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.META, "Composer",
                    it.groups[1]!!.value, it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = albumReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.META, "Album",
                    it.groups[1]!!.value, it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = lyricistReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.META, "Lyricist",
                    it.groups[1]!!.value, it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = timeReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.META, "Time",
                    it.groups[1]!!.value, it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = tempoReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.META, "Tempo",
                    it.groups[1]!!.value, it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        fres = durationReg.findAll(content)
        fres.forEach {
            val el = SongElement(SongElementType.META, "Duration",
                    it.groups[1]!!.value, it.groups[0]!!.value, it.groups[0]!!.range)
            elements.add(el)
        }
        elements.sortBy { el -> el.range.start }
        val textElements = mutableListOf<SongElement>()
        var start = 0
        var lastElem: SongElement? = null
        elements.forEach {
            lastElem = it
            val txt = content.substring(start, it.range.start)
            if (txt.isNotEmpty()) {
                val el = SongElement(SongElementType.TEXT, "", txt, "",
                        IntRange(start, it.range.start - 1))
                textElements.add(el)
            }
            start = it.range.last + 1
        }
        if (lastElem !== null && lastElem!!.range.endInclusive < content.length - 1) {
            val txt = content.substring(lastElem!!.range.endInclusive + 1, content.length - 1)
            val el = SongElement(SongElementType.TEXT, "", txt, "",
                    IntRange(lastElem!!.range.endInclusive + 1, content.length - 1))
            textElements.add(el)
        }
        if (elements.isEmpty() && content.isNotEmpty()) {
            // Add whole content because it does not containg marks
            textElements.add(SongElement(SongElementType.TEXT, "", content, content,
                    IntRange(0, content.length - 1)))
        }
        elements.addAll(textElements)
        elements.sortBy { el -> el.range.start }
        return elements
    }

    /**
     * Produce HTML from a list of SongElement objects.
     */
    fun toHtml(context: Context, elements: List<SongElement>): String {
        val sb = StringBuilder()
        foundChords.clear()
        var afterTitlePosition = 0
        sb.append("<html><head>").append(createStyles(context)).append("</head><body>")
        elements.forEach {
            when (it.type) {
                SongElementType.TEXT -> sb.append(htmlWithChords(it.content))
                SongElementType.TITLE -> {
                    sb.append("<div class=\"title\">").append(it.content).append("</div>")
                    afterTitlePosition = sb.length
                }
                SongElementType.SUBTITLE -> sb.append("<div class=\"subtitle\">").append(it.content).append("</div>")
                SongElementType.COMMENT -> sb.append("<div class=\"comment\">").append(it.content).append("</div>")
                SongElementType.META -> sb.append("<span class=\"metadata\">").append(it.title).append(": ").append(it.content).append("</span><br/>")
                SongElementType.CHORUS -> {
                    sb.append("<div class=\"chorus\">")
                    if (it.title.isNotEmpty()) {
                        sb.append("<b>").append(it.title).append("</b><br/>")
                    } else {
                        sb.append("<b>Chorus</b><br/>")
                    }
                    sb.append(htmlWithChords(it.content)).append("</div>")
                }
                SongElementType.VERSE -> {
                    sb.append("<div class=\"verse\">")
                    if (it.title.isNotEmpty()) {
                        sb.append("<b>").append(it.title).append("</b><br/>")
                    } else {
                        sb.append("<b>Verse</b><br/>")
                    }
                    sb.append(htmlWithChords(it.content)).append("</div>")
                }
                SongElementType.CHORD -> {
                    val cname = chordImages[it.title]
                    if (!cname.isNullOrEmpty()) {
                        sb.append("<img src=\"file:///android_asset/$cname.png\" /> ")
                    }
                }
            }
        }

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context.applicationContext)
        when (sharedPref.getString("pref_key_chord_autoinsert", "none")) {
            "top" -> {
                sb.insert(afterTitlePosition, "<div>" + autogeneratedChordsHtml() + "</div>")
            }
            "bottom" -> {
                sb.append("<div>").append(autogeneratedChordsHtml()).append("</div>")
            }
            else -> {
            }
        }
        sb.append("</body></html>")
        return sb.toString()
    }

    /**
     * Produce HTML for a text fragment with chord notation.
     */
    private fun htmlWithChords(text: String): String {
        val sb = StringBuilder()
        val lines = text.lines()
        val chordRegex = Regex("\\[([a-zA-Z][a-zA-Z0-9#+]*)]")

        val chords = mutableListOf<ChordElement>()
        var counter: Int = lines.size
        lines.forEach { line ->
            counter--
            val firstBracePosition: Int = line.indexOfFirst {
                it == '['
            }
            val lastBracePosition: Int = line.indexOfLast {
                it == ']'
            }
            val chordsAtEnd: Boolean = firstBracePosition in 1..(lastBracePosition - 1) && chordRegex.replace(line.substring(firstBracePosition, lastBracePosition + 1), "").trim().isEmpty()
            chords.clear()
            var printLine = chordRegex.replace(line, "")
            val mr = chordRegex.findAll(line)
            mr.forEach {
                val el = ChordElement(0, it.groups[1]!!.value, it.groups[0]!!.range)
                chords.add(el)
                if (!foundChords.contains(el.content))
                    foundChords.add(el.content)
            }
            chords.sortBy { e -> e.range.start }
            if (chordsAtEnd) {
                // All chords are at the end of line
                printLine = chordRegex.replace(line) {
                    "<span class=\"chord chordline\">" + it.groups[1]!!.value + "</span>"
                }
                printLine = printLine.substring(0, firstBracePosition) + "&nbsp;&nbsp;&nbsp;&nbsp;" + printLine.substring(firstBracePosition)
            }
            if (chords.size > 0 && !chordsAtEnd) {
                val chordLine = StringBuffer()
                var offset = 0
                chords.forEach {
                    chordLine.append("<span class=\"invisible\">")
                    if (offset <= it.range.start) {
                        val sl = chordRegex.replace(line.substring(offset, it.range.start), "")
                        chordLine.append(sl)
                    }
                    chordLine.append("</span>")
                    chordLine.append("<span class=\"chord\">").append(it.content).append("</span>")
                    offset += (it.range.last - offset + it.content.length + 1)
                }
                if (chordLine.trim().isNotEmpty() && printLine.trim().isNotEmpty()) {
                    sb.append("<span class=\"chordline\">").append(chordLine).append("<br/></span>")
                }
            }
            if (counter != 0 || printLine.isNotEmpty()) {
                sb.append(printLine).append("<br/>")
            }
        }
        return sb.toString()
    }

    private fun autogeneratedChordsHtml(): String {
        val sb = StringBuilder()
        foundChords.forEach {
            val cname = chordImages[it]
            if (!cname.isNullOrEmpty()) {
                sb.append("<img src=\"file:///android_asset/$cname.png\" /> ")
            }
        }
        return sb.toString()
    }

    fun getAllChords(text: String): List<String> {
        val result = mutableListOf<String>()
        val chordRegex = Regex("\\[([a-zA-Z][a-zA-Z0-9#+]*)]")
        val mr = chordRegex.findAll(text)
        mr.forEach {
            result.add(it.groups[1]!!.value)
        }
        return result
    }

}