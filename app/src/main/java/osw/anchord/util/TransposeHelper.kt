package osw.anchord.util

object TransposeHelper {

    val NORMALIZE_DICT = mapOf(
            "Cm" to "c", "Dm" to "d", "Em" to "e", "Fm" to "f", "Gm" to "g", "Am" to "a", "Bm" to "b", "Hm" to "h",
            "C#m" to "c#", "D#m" to "d#", "E#m" to "e#", "F#m" to "f#", "G#m" to "g#",
            "A#m" to "a#", "B#m" to "b#", "H#m" to "h#",
            "C7m" to "c7", "D7m" to "d7", "E7m" to "e7", "F7m" to "f7", "G7m" to "g7",
            "A7m" to "a7", "B7m" to "b7", "H7m" to "h7",
            "C6m" to "c6", "D6m" to "d6", "E6m" to "e6", "F6m" to "f6", "G6m" to "g6",
            "A6m" to "a6", "B6m" to "b6", "H6m" to "h6"
    )

    val MAJOR_SCALE_CHART = mapOf(
            "C" to arrayOf("C", "d", "e", "F", "G", "a", "B"),
            "D" to arrayOf("D", "e", "f", "G", "A", "h", "C"),
            "E" to arrayOf("E", "f", "g", "A", "H", "c", "D"),
            "F" to arrayOf("F", "g", "a", "B", "C", "d", "E"),
            "G" to arrayOf("G", "a", "h", "C", "D", "e", "F"),
            "A" to arrayOf("A", "h", "c", "D", "E", "f", "G"),
            "B" to arrayOf("B", "c", "d", "D", "F", "g", "A"),
            "H" to arrayOf("H", "c", "d", "E", "F", "g", "A")
    )

    val MINOR_SCALE_CHART = mapOf(
            "c" to arrayOf("c", "D", "D", "f", "g", "G", "B"),
            "d" to arrayOf("d", "E", "F", "g", "a", "B", "C"),
            "e" to arrayOf("e", "F", "G", "a", "h", "C", "D"),
            "f" to arrayOf("f", "G", "G", "b", "c", "E", "D"),
            "g" to arrayOf("g", "A", "B", "c", "d", "D", "F"),
            "a" to arrayOf("a", "H", "C", "d", "e", "F", "G"),
            "b" to arrayOf("b", "C", "E", "d", "f", "f", "G"),
            "h" to arrayOf("h", "C", "D", "e", "f", "G", "A")
    )


    fun convert(content: String, fromKey: String, toKey: String): String {
        var result = content
        val scale = MAJOR_SCALE_CHART[fromKey] ?: return ""
        val toscale = MAJOR_SCALE_CHART[toKey] ?: return ""
        val fromRegexTxt = scale.joinToString("|") { str ->
            "\\[\\s*($str[#]?)\\s*\\]"
        }
        val fromRegex = Regex(fromRegexTxt)
        result = result.replace(fromRegex) { mr ->
            var res = ""
            for (i in 1 until mr.groups.size) {
                if (mr.groups[i] != null) {
                    var final = toscale[scale.indexOf(mr.groups[i]!!.value.substring(0, 1))]
                    if (mr.groups[i]!!.value.endsWith("#")) {
                        final = "$final#"
                    }
                    res = "[$final]"
                }
            }
            res
        }
        return result
    }

    fun determineKey(chords: List<String>): String {
        val lst = chords.map { item ->
            val regex = Regex("([A-Za-z])[0-9]+")
            var result = regex.replace(item.trim(), "$1")
            NORMALIZE_DICT.keys.forEach {
                result = result.replace(it, NORMALIZE_DICT[it]!!)
            }
            result
        }
        val checkTab = mutableMapOf(
                "C" to 0, "D" to 0, "E" to 0, "F" to 0,
                "G" to 0, "A" to 0, "B" to 0, "H" to 0,
                "c" to 0, "d" to 0, "e" to 0, "f" to 0,
                "g" to 0, "a" to 0, "b" to 0, "h" to 0)
        chords.forEach { chord ->
            MAJOR_SCALE_CHART.keys.forEach { key ->
                if (MAJOR_SCALE_CHART[key]!!.map { item -> item.substring(0, 1) }.contains(chord.substring(0, 1))) {
                    checkTab[key] = checkTab[key]!! + 1
                }
            }
            MINOR_SCALE_CHART.keys.forEach { key ->
                if (MINOR_SCALE_CHART[key]!!.map { item -> item.substring(0, 1) }.contains(chord.substring(0, 1))) {
                    checkTab[key] = checkTab[key]!! + 1
                }
            }
        }
        val sorted = checkTab.toList().sortedBy { (_, value) -> value }.asReversed().toMap()
        return sorted.keys.filter { key -> key == key.toUpperCase() }.first()
    }
}