package osw.anchord

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*
import osw.anchord.db.AppDatabase
import osw.anchord.db.DbWorker
import osw.anchord.db.entities.Song
import osw.anchord.fragments.SettingsFragment
import osw.anchord.fragments.SongViewFragment
import osw.anchord.fragments.chordlist.ChordListFragment
import osw.anchord.fragments.songlist.SongListFragment
import osw.anchord.fragments.websearch.WebItemFragment
import osw.anchord.fragments.websearch.WebItemViewFragment
import osw.anchord.ui.ColourView
import osw.anchord.util.*


class MainActivity : AppCompatActivity() {


    companion object {
        const val IMPORT_SONG_CODE: Int = 100
    }

    // Song list is the main fragment of the application
    private lateinit var songListFragment: SongListFragment
    // Settings
    private lateinit var settingsFragment: SettingsFragment
    // Chord list
    private lateinit var chordListFragment: ChordListFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        this.title = ""

        DbWorker.getInstance()!!.start()
        NetWorker.getInstance()!!.start()
        songListFragment = SongListFragment()
        settingsFragment = SettingsFragment()
        chordListFragment = ChordListFragment()
        switchFragment(songListFragment, false)
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        switchFragment(songListFragment, false)
    }

    override fun onBackPressed() {
        when {
            currentFragment() is ChordListFragment -> {
                switchFragment(songListFragment, false)
            }
            else -> super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_about -> {
                val b = AlertDialog.Builder(this)
                b.setTitle("AnChord")
                b.setMessage("ChordPro Songbook.\n2018 Oskar Świda\noskar.swida@gmail.com")
                b.create().show()
                true
            }
            R.id.action_settings -> {
                switchFragment(settingsFragment, true)
                true
            }
            R.id.action_create_song -> {
                val builder = AlertDialog.Builder(this)
                with(builder) {
                    setTitle(getString(R.string.action_create_song))
                    val edit = EditText(context)
                    edit.hint = getString(R.string.song_title)
                    edit.setPadding(15, 15, 15, 15)
                    setView(edit)
                    setPositiveButton(getString(R.string.create)) { dialog, _ ->
                        dialog.dismiss()
                        if (edit.text.toString().isNotEmpty()) {
                            val song = Song(null, SongHelper.emptySongContent.format(edit.text.toString()),
                                    edit.text.toString(), "")
                            DbHelper.postDbTask(applicationContext) { db ->
                                db.songDao().insertAll(song)
                            }
                            (currentFragment() as SongListFragment).refresh()
                        }
                    }
                    setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                        dialog.dismiss()
                    }
                    create().show()
                }
                true
            }
            R.id.action_chord_list -> {
                switchFragment(chordListFragment, true)
                true
            }
            R.id.action_websearch -> {
                if (WebSearchHelper.hasNetwork(this)) {
                    switchFragment(WebItemFragment.newInstance(), true)
                } else {
                    Dialogs.info(this, getString(R.string.error), getString(R.string.no_net_msg))
                }
                true
            }
            R.id.action_export -> {
                if (FileHelper.isExternalStorageWritable()) {
                    DbHelper.postDbTask(this) { db ->
                        val list = db.songDao().getAll()
                        list.forEach { song ->
                            FileHelper.saveSongToAppDir(this, "${song.title}.txt", song.content!!)
                        }
                        Dialogs.info(this, getString(R.string.songs_export), getString(R.string.songs_export_message))
                    }
                } else {
                    Dialogs.info(this, getString(R.string.error), getString(R.string.no_external_storage))
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                MainActivity.IMPORT_SONG_CODE -> {
                    if (resultData!!.clipData != null) {
                        val clipData = resultData.clipData
                        for (i in 0 until clipData.itemCount) {
                            val stext = FileHelper.readTextFromUri(contentResolver, clipData.getItemAt(i).uri)
                            val song = SongHelper.importSong(stext)
                            DbHelper.postDbTask(this) { db ->
                                db.songDao().insertAll(song)
                            }
                        }
                    } else if (resultData.data != null) {
                        val stext = FileHelper.readTextFromUri(contentResolver, resultData.data)
                        val song = SongHelper.importSong(stext)
                        DbHelper.postDbTask(this) { db ->
                            db.songDao().insertAll(song)
                        }
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        AppDatabase.destroyInstance()
        DbWorker.getInstance()!!.quit()
        NetWorker.getInstance()!!.quit()
        super.onDestroy()
    }

    fun onStarClick(v: View) {
        val builder = AlertDialog.Builder(this)
        with(builder) {
            val vv = ColourView(context, v.tag as Song)
            setTitle(getString(R.string.star_select))
            vv.setPadding(15, 15, 15, 15)
            setView(vv)
            val dlg = create()
            vv.dialog = dlg
            dlg.setOnDismissListener {
                (currentFragment() as SongListFragment).refresh()
            }
            dlg.show()
        }
    }


    /**
     * Switch fragment.
     */
    fun switchFragment(fragment: Fragment, addToBackStack: Boolean) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, fragment)
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null)
        }
        fragmentTransaction.commit()
    }

    /**
     * Get current active (displayed) fragment
     */
    private fun currentFragment(): Fragment {
        return supportFragmentManager.findFragmentById(R.id.fragment_container)
    }

    fun switchToWebItemView(title: String, content: String) {
        switchFragment(WebItemViewFragment.newInstance(title, content), true)
    }


    fun switchToSongList() {
        switchFragment(songListFragment, false)
        songListFragment.refresh()
    }

    fun switchToSongView(title: String, content: String, id: Long) {
        switchFragment(SongViewFragment.newInstance(title, content, id), false)
    }

    fun runImportSong() {
        val itn = Intent(Intent.ACTION_OPEN_DOCUMENT)
        itn.addCategory(Intent.CATEGORY_OPENABLE)
        itn.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        itn.type = "text/*"
        startActivityForResult(itn, MainActivity.IMPORT_SONG_CODE)
        true
    }

}

