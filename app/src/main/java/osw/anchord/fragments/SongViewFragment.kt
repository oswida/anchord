package osw.anchord.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.preference.PreferenceManager
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Spinner
import android.widget.TextView
import osw.anchord.MainActivity
import osw.anchord.R
import osw.anchord.util.DbHelper
import osw.anchord.util.SongHelper
import osw.anchord.util.TransposeHelper

private const val ARG_CONTENT = "content"
private const val ARG_TITLE = "title"
private const val ARG_ID = "id"


/**
 * Fragment to show song details.
 */
class SongViewFragment : Fragment() {

    private var webview: WebView? = null
    private var menu: Menu? = null
    private var playing: Boolean = false
    private var lastData: String? = null
    private var nightMode: Boolean = false
    private var viewChords: Boolean = true
    private var content: String? = null
    private var title: String? = null
    private var id: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            content = it.getString(ARG_CONTENT)
            title = it.getString(ARG_TITLE)
            id = it.getLong((ARG_ID))
        }
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity as MainActivity)
        nightMode = sharedPref.getBoolean("pref_key_nightmode", false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_song_view, container, false)
        webview = v.findViewById(R.id.song_view)
        webview!!.settings.javaScriptEnabled = true
        setHasOptionsMenu(true)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateSong()
    }

    override fun onResume() {
        super.onResume()
        playing = false
    }

    fun updateSong() {
        Log.i("context", context?.toString())
        Log.i("title", title?.toString())
        Log.i("content", content?.toString())

        lastData = SongHelper.toHtml(context!!, SongHelper.parseSong(content!!))
        if (lastData != null) {
            val mode = menu?.findItem(R.id.action_nightmode)
            val regex = Regex("body\\s*(\\{\\s*font-size:\\s*([0-9]+)pt[^\\}]*\\})")
            var mr = regex.find(lastData!!)
            if (mr != null) {
                val bodySize = mr.groups[2]!!.value.toInt()
                if (nightMode) {
                    mode?.setIcon(R.drawable.ic_action_mode_day)
                    lastData = lastData!!.replaceRange(mr.groups[1]!!.range, "{ font-size:%spt; color:white; background-color:#141d26; }".format(java.util.Locale.US, bodySize))
                } else {
                    mode?.setIcon(R.drawable.ic_action_mode_night)
                    lastData = lastData!!.replaceRange(mr.groups[1]!!.range, "{ font-size:%spt; }".format(java.util.Locale.US, bodySize))
                }
                val imgregex = Regex("img\\s*(\\{[^\\}]*\\})")
                val ir = imgregex.find(lastData!!)
                if (ir != null) {
                    if (nightMode) {
                        lastData = lastData!!.replaceRange(ir.groups[1]!!.range, "{ -webkit-filter: invert(1); filter: invert(1); }")
                    } else {
                        lastData = lastData!!.replaceRange(ir.groups[1]!!.range, "{ }")
                    }
                }
                webview?.loadDataWithBaseURL("", lastData, "text/html", "UTF-8", "")
            }
        }
        webview?.loadDataWithBaseURL("", lastData, "text/html", "UTF-8", "")
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_song, menu)
        this.menu = menu
        val mode = menu?.findItem(R.id.action_nightmode)
        if (nightMode) {
            mode?.setIcon(R.drawable.ic_action_mode_day)
        } else {
            mode?.setIcon(R.drawable.ic_action_mode_night)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            R.id.action_play -> {
                val play = menu!!.findItem(R.id.action_play)
                playing = !playing
                if (playing) {
                    val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity as MainActivity)
                    webview!!.evaluateJavascript("""
                |var _fullScrollIntervalID = setInterval(function() {
                |    if (window.scrollY + window.innerHeight >= document.body.scrollHeight) {
                |        window.clearInterval(_fullScrollIntervalID);
                |    } else {
                |        window.scrollBy(0, %s);
                |    }
                |}, 1000*%s);
            """.format(sharedPref.getString("pref_key_text_size", "14"),
                            sharedPref.getString("pref_key_autoscroll_secs", "0.5")).trimMargin(), null)
                    play.setIcon(R.drawable.ic_action_stop)
                } else {
                    webview!!.evaluateJavascript("window.clearInterval(_fullScrollIntervalID);".trimMargin(), null)
                    play.setIcon(R.drawable.ic_action_play)
                }
                true
            }
            R.id.action_wrap -> {
                webview!!.settings.useWideViewPort = !webview!!.settings.useWideViewPort
                val wrap = menu?.findItem(R.id.action_wrap)
                if (webview!!.settings.useWideViewPort) {
                    wrap!!.setIcon(R.drawable.ic_action_wrap)
                } else {
                    wrap!!.setIcon(R.drawable.ic_action_unwrap)
                }
                true
            }
            R.id.action_zoomin -> {
                zoom(1)
                true
            }
            R.id.action_zoomout -> {
                zoom(-1)
                true
            }
            R.id.action_nightmode -> {
                nightMode = !nightMode
                updateSong()
                true
            }
            R.id.action_toggle_chords -> {
                viewChords = !viewChords
                toggleChords()
                true
            }
            R.id.action_edit -> {
                val frag = SongEditFragment.newInstance(title!!, content!!, id!!)
                (activity as MainActivity).switchFragment(frag, true)
                true
            }
            R.id.action_transpose -> {
                val key = TransposeHelper.determineKey(SongHelper.getAllChords(content!!))

                val builder = AlertDialog.Builder(context)
                with(builder) {
                    setTitle(getString(R.string.transpose_song))
                    val fromTitle = TextView(context)
                    fromTitle.text = getString(R.string.from_key)
                    val fromEdit = Spinner(context)
                    fromEdit.adapter = ArrayAdapter(context,
                            android.R.layout.simple_spinner_dropdown_item, SongHelper.CHORD_NAMES)
                    fromEdit.setSelection(SongHelper.CHORD_NAMES.indexOf(key))
                    val toTitle = TextView(context)
                    toTitle.text = getString(R.string.to_key)
                    val toEdit = Spinner(context)
                    toEdit.adapter = ArrayAdapter(context,
                            android.R.layout.simple_spinner_dropdown_item, SongHelper.CHORD_NAMES)
                    val view = LinearLayout(context)
                    view.layoutParams = ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    view.orientation = LinearLayout.VERTICAL
                    view.setPadding(20, 20, 20, 20)
                    view.addView(fromTitle)
                    view.addView(fromEdit)
                    view.addView(toTitle)
                    view.addView(toEdit)
                    setView(view)
                    setNegativeButton(getString(R.string.cancel)) { dialog, which ->
                        dialog.dismiss()
                    }
                    setPositiveButton(getString(R.string.transpose)) { dialog, which ->
                        dialog.dismiss()
                        val result = TransposeHelper.convert(content!!,
                                fromEdit.selectedItem.toString(), toEdit.selectedItem.toString())
                        DbHelper.postDbTask(context) { db ->
                            val song = db.songDao().findById(id!!)
                            song.content = result
                            db.songDao().save(song)
                        }
                        updateSong()
                    }
                    create().show()
                }
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun zoom(coeff: Int) {
        if (lastData != null) {
            val regex = Regex("body\\s*\\{\\s*font-size:\\s*([0-9]+)pt[\\s;]*\\}")
            var mr = regex.find(lastData!!)
            if (mr != null) {
                val sz = mr.groups[1]!!.value.toInt() + coeff
                lastData = lastData!!.replaceRange(mr.groups[1]!!.range, sz.toString())
                webview?.loadDataWithBaseURL("", lastData, "text/html", "UTF-8", "")
            }
        }
    }

    private fun toggleChords() {
        if (lastData != null) {
            val regex = Regex(".chordline\\s*\\{([^\\{\\}]*)\\}")
            var mr = regex.find(lastData!!)
            if (mr != null) {
                var txt = " "
                if (!viewChords) {
                    txt = "display:none"
                }
                lastData = lastData!!.replaceRange(mr.groups[1]!!.range, txt)
                webview?.loadDataWithBaseURL("", lastData, "text/html", "UTF-8", "")
            }
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(title: String, content: String, id: Long) =
                SongViewFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_CONTENT, content)
                        putString(ARG_TITLE, title)
                        putLong(ARG_ID, id)
                    }
                }
    }

}
