package osw.anchord.fragments.songlist

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import osw.anchord.db.entities.Song
import osw.anchord.util.AppSettings
import osw.anchord.util.DbHelper

/**
 * Model for Song list view.
 */
class SongListViewModel(val app: Application) : AndroidViewModel(app) {

    var songList: MutableLiveData<List<Song>> = MutableLiveData<List<Song>>()

    init {
        DbHelper.postDbTask(app.applicationContext) { db ->
            songList.postValue(db.songDao().getAll())
        }
    }

    fun refresh(filter: String) {
        DbHelper.postDbTask(app.applicationContext) { db ->
            if (!filter.trim().isNullOrEmpty() && AppSettings.selectedStars.size > 0) {
                songList.postValue(db.songDao().findByTitleAndColor("$filter%", AppSettings.selectedStars))
            } else if (!filter.trim().isNullOrEmpty()) {
                songList.postValue(db.songDao().findByTitle("$filter%"))
            } else if (AppSettings.selectedStars.size > 0) {
                songList.postValue(db.songDao().findByColor(AppSettings.selectedStars))
            } else {
                songList.postValue(db.songDao().getAll())
            }
        }
    }

}