package osw.anchord.fragments.songlist


import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_song.view.*
import osw.anchord.R
import osw.anchord.db.entities.Song
import osw.anchord.util.ColorHelper

/**
 * [RecyclerView.Adapter] that can display a [Song] and makes a call to the
 * specified [SongListListener].
 */
class SongListRecyclerViewAdapter(val fragment: SongListFragment)
    : RecyclerView.Adapter<SongListRecyclerViewAdapter.ViewHolder>() {

    var mValues = mutableListOf<Song>()
    private val mOnClickListener: View.OnClickListener
    private val mOnLongClickListener: View.OnLongClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Song
            fragment.onSongClick(item)
        }
        mOnLongClickListener = View.OnLongClickListener { v ->
            val item = v.tag as Song
            fragment.onSongLongClick(item)!!
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_song, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues?.get(position)
        holder.mTitleView.text = item?.title
        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
            setOnLongClickListener(mOnLongClickListener)
        }
        holder.mStarView.tag = item
        when (item!!.color) {
            "red" -> {
                holder.mStarView.setImageResource(R.drawable.ic_category)
                holder.mStarView.setColorFilter(ColorHelper.red(fragment.context!!))
            }
            "green" -> {
                holder.mStarView.setImageResource(R.drawable.ic_category)
                holder.mStarView.setColorFilter(ColorHelper.green(fragment.context!!))
            }
            "blue" -> {
                holder.mStarView.setImageResource(R.drawable.ic_category)
                holder.mStarView.setColorFilter(ColorHelper.blue(fragment.context!!))
            }
            "black" -> {
                holder.mStarView.setImageResource(R.drawable.ic_category)
                holder.mStarView.setColorFilter(ColorHelper.black(fragment.context!!))
            }
            "yellow" -> {
                holder.mStarView.setImageResource(R.drawable.ic_category)
                holder.mStarView.setColorFilter(ColorHelper.yellow(fragment.context!!))
            }
            "magenta" -> {
                holder.mStarView.setImageResource(R.drawable.ic_category)
                holder.mStarView.setColorFilter(ColorHelper.magenta(fragment.context!!))
            }
            else -> {
                holder.mStarView.setImageResource(R.drawable.ic_category_empty)
                holder.mStarView.clearColorFilter()
            }

        }
    }

    override fun getItemCount(): Int = mValues!!.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mTitleView: TextView = mView.song_title
        val mStarView: ImageView = mView.song_star
        val mCheckView: CheckBox = mView.song_check

        override fun toString(): String {
            return super.toString() + " '" + mTitleView.text + "'"
        }
    }
}
