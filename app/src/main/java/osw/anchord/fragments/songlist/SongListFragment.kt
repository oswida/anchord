package osw.anchord.fragments.songlist

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.EditText
import osw.anchord.MainActivity
import osw.anchord.R
import osw.anchord.db.entities.Song
import osw.anchord.fragments.SongViewFragment
import osw.anchord.ui.ColourFilterView
import osw.anchord.util.AppSettings
import osw.anchord.util.DbHelper
import osw.anchord.util.FileHelper
import osw.anchord.util.SongHelper

/**
 * A fragment representing a list of Items.
 */
class SongListFragment : Fragment() {

    private var model: SongListViewModel? = null
    var filter: String = ""
    private var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        (activity as MainActivity).title = ""
        model = ViewModelProviders.of(this).get(SongListViewModel::class.java)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_main, menu)
        val searchView = menu!!.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isNullOrEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                refresh()
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_sample -> {
                val sampleData = FileHelper.readTextFromAsset((activity as MainActivity).assets, "example.crd")
                val song = SongHelper.importSong(sampleData)
                DbHelper.postDbTask(context!!) { db ->
                    db.songDao().insertAll(song)
                    refresh()
                }
                true
            }
            R.id.action_select_all -> {
                for (i in 0 until recyclerView!!.childCount) {
                    val vh = recyclerView!!.getChildViewHolder(
                            recyclerView!!.getChildAt(i)) as SongListRecyclerViewAdapter.ViewHolder
                    vh.mCheckView.isChecked = !vh.mCheckView.isChecked
                }
                true
            }
            R.id.action_delete -> {
                val list = mutableListOf<Song>()
                for (i in 0 until recyclerView!!.childCount) {
                    val vh = recyclerView!!
                            .getChildViewHolder(
                                    recyclerView!!.getChildAt(i)) as SongListRecyclerViewAdapter.ViewHolder
                    if (vh.mCheckView.isChecked) {
                        list.add(vh.mView.tag as Song)
                    }
                }
                if (list.size > 0) {
                    val builder = AlertDialog.Builder(context)
                    with(builder) {
                        setTitle("Delete songs")
                        setMessage("Do you want to delete " + list.size.toString() + " selected songs?")
                        setPositiveButton("Delete") { dialog, _ ->
                            dialog.dismiss()
                            for (i in 0 until recyclerView!!.childCount) {
                                val vh = recyclerView!!.getChildViewHolder(
                                        recyclerView!!.getChildAt(i)) as SongListRecyclerViewAdapter.ViewHolder
                                vh.mCheckView.isChecked = false
                            }
                            DbHelper.postDbTask(context!!) { db ->
                                list.forEach {
                                    db.songDao().delete(it)
                                }
                                refresh()
                            }
                        }
                        setNegativeButton("Cancel") { dialog, _ ->
                            dialog.dismiss()
                        }
                    }
                    builder.create().show()
                }
                true
            }
            R.id.action_stars -> {
                val builder = AlertDialog.Builder(context)
                with(builder) {
                    val vv = ColourFilterView(context)
                    setTitle(getString(R.string.star_filter))
                    vv.setPadding(15, 15, 15, 15)
                    setView(vv)
                    val dlg = create()
                    vv.dialog = dlg
                    dlg.setOnDismissListener {
                        val clrs = vv.selected.filter {
                            it.value
                        }.map {
                            it.key
                        }
                        AppSettings.selectedStars.clear()
                        AppSettings.selectedStars.addAll(clrs)
                        refresh()
                    }
                    dlg.show()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        model!!.refresh(filter)
    }

    fun refresh() {
        model!!.refresh(filter)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val vv = inflater.inflate(R.layout.fragment_song_list, container, false)
        recyclerView = vv.findViewById<RecyclerView>(R.id.list)
        // Set the adapter
        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            adapter = SongListRecyclerViewAdapter(this@SongListFragment)
        }
        model!!.songList.observe(this, Observer<List<Song>> {
            with(recyclerView!!) {
                val ad = adapter as SongListRecyclerViewAdapter
                ad.mValues!!.clear()
                ad.mValues!!.addAll(it!!)
                ad.notifyDataSetChanged()
            }
        })
        val fab = vv.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            (activity as MainActivity).runImportSong()
            true
        }
        return vv
    }


    fun onSongClick(item: Song) {
        val fragment = SongViewFragment.newInstance(item!!.title!!, item.content!!, item.id!!)
        (activity as MainActivity).switchFragment(fragment, true)
    }

    fun onSongLongClick(item: Song): Boolean? {
        val builder = AlertDialog.Builder(context)
        with(builder) {
            setTitle(getString(R.string.change_song_title))
            val edit = EditText(context)
            edit.setText(item!!.title)
            setView(edit)
            setPositiveButton(getString(R.string.change)) { dialog, _ ->
                dialog.dismiss()
                val newtitle = edit.text.toString().trim()
                if (newtitle.isNotEmpty()) {
                    item.title = newtitle
                    val titleReg = Regex("\\{(?:title|t):\\s*([a-zA-Z0-9_ \\-]*)\\s*\\}\\s*\\n",
                            RegexOption.IGNORE_CASE)
                    item.content = titleReg.replace(item.content!!, "{title: $newtitle}\n")
                    DbHelper.postDbTask(context) { db ->
                        db.songDao().save(item)
                        refresh()
                    }
                }
            }
            setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                dialog.dismiss()

            }
            create().show()
        }
        return true
    }

}
