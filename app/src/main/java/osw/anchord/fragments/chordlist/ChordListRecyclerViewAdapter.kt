package osw.anchord.fragments.chordlist


import android.content.res.AssetManager
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import kotlinx.android.synthetic.main.chord.view.*
import osw.anchord.MainActivity
import osw.anchord.R


class ChordListRecyclerViewAdapter()
    : RecyclerView.Adapter<ChordListRecyclerViewAdapter.ViewHolder>() {

    var mValues = mutableListOf<String>()
    lateinit var assets: AssetManager
    private val chords = listOf("A", "A6", "A7", "A9", "Am", "Am6", "Am7", "Aplus",
            "a", "a6", "a7",
            "B", "B6", "B7", "B9", "Bm", "Bm6", "Bm7", "Bplus",
            "b", "b6", "b7",
            "C", "C6", "C7", "C9", "Cm", "Cm6", "Cm7", "Cplus",
            "c", "c6", "c7",
            "D", "D6", "D7", "D9", "Dm", "Dm6", "Dm7", "Dplus",
            "d", "d6", "d7",
            "E", "E6", "E7", "E9", "Em", "Em6", "Em7", "Eplus",
            "e", "e6", "e7",
            "F", "F6", "F7", "F9", "Fm", "Fm6", "Fm7", "Fplus", "Fhash", "Fmhash",
            "f", "f6", "f7", "fhash",
            "G", "G6", "G7", "G9", "Gm", "Gm6", "Gm7", "Gplus",
            "g", "g6", "g7",
            "H", "H6", "H7", "H9", "Hm", "Hm6", "Hm7", "Hplus",
            "h", "h6", "h7")
    private val chordLetters = arrayOf("A", "a", "B", "b", "C", "c", "D", "d", "E", "e",
            "F", "f", "G", "g", "H", "h")

    init {
        mValues.clear()
        mValues.addAll(chords)
    }

    fun filter(checked: BooleanArray) {
        val list = mutableListOf<String>()
        for (i in 0 until checked.size) {
            if (checked[i])
                list.add(chordLetters[i])
        }
        mValues.clear()
        if (list.size > 0) {
            mValues.addAll(chords.filter {
                it.substring(0, 1) in list
            })
        } else {
            mValues.addAll(chords)
        }
        notifyDataSetChanged()
    }

    fun clearFilter() {
        mValues.clear()
        mValues.addAll(chords)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.chord, parent, false)
        assets = parent.context.applicationContext.assets
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position >= mValues.size) return
        val item = mValues.get(position)
        if (!item.isNullOrEmpty()) {
            var dr = Drawable.createFromStream(assets.open("$item.png"), null)
            holder.mImage.setImageDrawable(dr)
        }
        with(holder.mView) {
            tag = item
        }
        holder.mImage.tag = item
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mImage: ImageView = mView.chord_image

        override fun toString(): String {
            return super.toString() + " '" + mImage.tag + "'"
        }
    }
}

