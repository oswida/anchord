package osw.anchord.fragments.chordlist


import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.preference.PreferenceManager
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import osw.anchord.MainActivity
import osw.anchord.R


/**
 * Chord list.
 *
 */
class ChordListFragment : Fragment() {

    private val chordLetters = arrayOf("A", "a", "B", "b", "C", "c", "D", "d", "E", "e",
            "F", "f", "G", "g", "H", "h")
    private val checkedLetters = BooleanArray(chordLetters.size)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_chord_list, container, false)
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity as MainActivity)
        val columns = sharedPref.getString("pref_key_chordlist_cols", "2").toInt()
        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = GridLayoutManager(context, columns)
                adapter = ChordListRecyclerViewAdapter()
            }
        }
        setHasOptionsMenu(true)
        (activity as MainActivity).title = getString(R.string.guitar_chords)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_chords, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_filter_chords -> {
                val builder = AlertDialog.Builder(context)
                with(builder) {
                    setTitle(getString(R.string.filter_chords))
                    setMultiChoiceItems(chordLetters, checkedLetters) { _, _, _ ->
                        with(view as RecyclerView) {
                            (adapter as ChordListRecyclerViewAdapter).filter(checkedLetters)
                        }
                    }
                    setPositiveButton(getString(R.string.close)) { _, _ -> }
                    setNeutralButton(getString(R.string.clear)) { _, _ ->
                        for (i in 0 until checkedLetters.size) checkedLetters[i] = false
                        with(view as RecyclerView) {
                            (adapter as ChordListRecyclerViewAdapter).clearFilter()
                        }
                    }
                    var dlg = create()
                    dlg.show()
                    dlg.window.setLayout(400, -2)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
