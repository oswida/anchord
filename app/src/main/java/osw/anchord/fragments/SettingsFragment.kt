package osw.anchord.fragments

import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat
import android.view.Menu
import osw.anchord.R
import android.view.MenuInflater


class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_settings, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}
