package osw.anchord.fragments

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.preference.PreferenceManager
import android.text.Editable
import android.text.Spannable
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import osw.anchord.MainActivity
import osw.anchord.R
import osw.anchord.ui.PredefEditAdapter
import osw.anchord.util.DbHelper

private const val ARG_CONTENT = "content"
private const val ARG_TITLE = "title"
private const val ARG_ID = "id"

class SongEditFragment : Fragment() {

    var editArea: EditText? = null
    private var content: String? = null
    private var title: String? = null
    private var id: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            content = it.getString(ARG_CONTENT)
            title = it.getString(ARG_TITLE)
            id = it.getLong((ARG_ID))
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val vw = inflater.inflate(R.layout.fragment_song_edit, container, false)
        val tt = vw.findViewById<TextView>(R.id.song_edit_title)
        tt.text = title!!
        (activity as MainActivity).title = ""
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity as MainActivity)
        editArea = vw.findViewById(R.id.song_edit_area)
        editArea!!.setTextSize(TypedValue.COMPLEX_UNIT_PT, sharedPref.getString("pref_key_edit_text_size", "14").toFloat())
        editArea!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val directiveRegex = Regex("\\{([a-zA-Z0-9]+)(:[^\\{\\}]*)?\\}")
                var mr = directiveRegex.findAll(s.toString())
                mr.forEach {
                    val dt = it.groups[1]?.value
                    if (dt != null) {
                        s!!.setSpan(ForegroundColorSpan(Color.parseColor("blue")),
                                it.groups[1]!!.range.start,
                                it.groups[1]!!.range.endInclusive + 1,
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    }
                    val dv = it.groups[2]?.value
                    if (dv != null) {
                        s!!.setSpan(ForegroundColorSpan(Color.parseColor("magenta")),
                                it.groups[2]!!.range.start,
                                it.groups[2]!!.range.endInclusive + 1,
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    }
                }
                val chordRegex = Regex("\\[[\\sa-zA-Z0-9#+]+\\]")
                mr = chordRegex.findAll(s.toString())
                mr.forEach {
                    val ct = it.groups[0]?.value
                    if (ct != null) {
                        s!!.setSpan(ForegroundColorSpan(Color.parseColor("red")),
                                it.groups[0]!!.range.start,
                                it.groups[0]!!.range.endInclusive + 1,
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        editArea!!.text.replace(0, editArea!!.text.length, content)
        return vw
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_song_edit, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_save -> {
                DbHelper.postDbTask(context!!) { db ->
                    val song = db.songDao().findById(id!!)
                    song!!.content = editArea!!.text.toString()
                    val titleReg = Regex("\\{(?:title|t):\\s*([a-zA-Z0-9_ \\-]*)\\s*\\}\\s*\\n", RegexOption.IGNORE_CASE)
                    val mr = titleReg.find(song.content!!)
                    if (mr != null) {
                        song.title = mr.groups[1]!!.value
                    }
                    db.songDao().save(song)
                    val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view!!.windowToken, 0)
                    (activity as MainActivity).switchToSongView(song.title!!, song.content!!, song.id!!)
                }
                true
            }
            R.id.action_cancel_edit -> {
                val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view!!.windowToken, 0)
                (activity as MainActivity).switchToSongView(title!!, content!!, id!!)
                true
            }
            R.id.action_edit_insert_item -> {
                val builder = AlertDialog.Builder(context)
                with(builder) {
                    setTitle(getString(R.string.insert_predefined))
                    val adapter = PredefEditAdapter(context)
                    setSingleChoiceItems(adapter, android.R.layout.simple_list_item_1) { dialog, which ->
                        val data = adapter.itemAt(which)
                        val key = adapter.keyAt(which)
                        val pos = editArea!!.selectionStart
                        editArea!!.text.insert(pos, data)
                        editArea!!.setSelection(pos + adapter.offsets[key]!!)
                        dialog.dismiss()
                    }
                    create().show()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(title: String, content: String, id: Long) =
                SongEditFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_CONTENT, content)
                        putString(ARG_TITLE, title)
                        putLong(ARG_ID, id)
                    }
                }
    }

}
