package osw.anchord.fragments.websearch

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.view.inputmethod.InputMethodManager
import osw.anchord.MainActivity
import osw.anchord.R
import osw.anchord.util.NetWorker
import osw.anchord.util.WebSearchHelper

class WebItemFragment : Fragment() {

    var filter = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_webitem_list, container, false)
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
                adapter = WebItemRecyclerViewAdapter(this@WebItemFragment)
            }
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        activity!!.title = resources.getString(R.string.websearch)
        if (filter.isNotEmpty()) {
            searchForTitle()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_websearch, menu)
        val searchView = menu!!.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isNullOrEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.isIconified = false
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                filter = query ?: ""
                val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view!!.windowToken, 0)
                searchForTitle()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })
    }

    private fun searchForTitle() {
        if (filter.isNotEmpty()) {
            NetWorker.getInstance()!!.postTask(Runnable {
                val result = WebSearchHelper.findTitle(filter)
                activity!!.runOnUiThread {
                    with(view as RecyclerView) {
                        (adapter as WebItemRecyclerViewAdapter).mValues.clear()
                        (adapter as WebItemRecyclerViewAdapter).mValues.addAll(result)
                        (adapter as WebItemRecyclerViewAdapter).notifyDataSetChanged()
                    }
                }
            })
        }
    }

    fun onItemClick(item: WebItem) {
        NetWorker.getInstance()!!.postTask(Runnable {
            val s = WebSearchHelper.extractSong(item.url)
            (activity as MainActivity).switchToWebItemView(item.name, s)
        })
    }

    companion object {

        @JvmStatic
        fun newInstance() =
                WebItemFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }
}
