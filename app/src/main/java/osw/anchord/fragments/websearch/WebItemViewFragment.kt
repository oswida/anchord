package osw.anchord.fragments.websearch


import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import osw.anchord.MainActivity
import osw.anchord.R
import osw.anchord.db.entities.Song
import osw.anchord.util.DbHelper
import osw.anchord.util.Dialogs

private const val ARG_CONTENT = "content"
private const val ARG_TITLE = "title"

class WebItemViewFragment : Fragment() {

    private var content: String? = null
    private var title: String? = null
    private var contentView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            content = it.getString(ARG_CONTENT)
            title = it.getString(ARG_TITLE)
        }
        activity!!.title = title
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_web_item_view, container, false)
        contentView = view.findViewById(R.id.webitem_content)
        contentView!!.text = content
        val fab = view.findViewById<FloatingActionButton>(R.id.webitem_fab)
        fab.setOnClickListener {
            Dialogs.inputString(context, resources.getString(R.string.import_song), title!!) {
                DbHelper.postDbTask(context!!) { db ->
                    val txt = "{title: $it}\n" + contentView!!.text.toString()
                    val song = Song(null, txt, it, "")
                    db.songDao().insertAll(song)
                    (activity as MainActivity).switchToSongList()
                }
            }
        }
        return view
    }


    companion object {

        @JvmStatic
        fun newInstance(title: String, content: String) =
                WebItemViewFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_CONTENT, content)
                        putString(ARG_TITLE, title)
                    }
                }
    }
}
