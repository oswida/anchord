package osw.anchord.fragments.websearch

data class WebItem(val name:String, val url:String)
