package osw.anchord.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Song entity.
 */
@Entity(tableName = "song")
data class Song(@PrimaryKey(autoGenerate = true) var id: Long?,
                @ColumnInfo(name = "content") var content: String?,
                @ColumnInfo(name = "title") var title: String?,
                @ColumnInfo(name = "color") var color: String?) {
    constructor() : this(null, "", "", "")
}
