package osw.anchord.db.entities

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

/**
 * Song DAO.
 */
@Dao
interface SongDao {
    @Query("SELECT * FROM song")
    fun getAll(): List<Song>

    @Query("SELECT * FROM song WHERE title LIKE :first AND color IN (:colors)")
    fun findByTitleAndColor(first: String, colors: List<String>): List<Song>

    @Query("SELECT * FROM song WHERE color IN (:colors)")
    fun findByColor(colors: List<String>): List<Song>

    @Query("SELECT * FROM song WHERE title LIKE :first")
    fun findByTitle(first: String): List<Song>

    @Query("SELECT * FROM song WHERE id = :id")
    fun findById(id: Long): Song

    @Query("SELECT * FROM song WHERE id IN (:songIds)")
    fun loadAllByIds(songIds: IntArray): List<Song>

    @Insert
    fun insertAll(vararg songs: Song)

    @Delete
    fun delete(song: Song)

    @Update
    fun save(song: Song)

}