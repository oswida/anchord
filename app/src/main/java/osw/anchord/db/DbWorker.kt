package osw.anchord.db

import android.content.Context
import android.os.Handler
import android.os.HandlerThread

/**
 * Database worker. Thread handler to avoid calling database operations in GUI thread.
 */
class DbWorker(name: String) : HandlerThread(name) {

    private lateinit var mWorkerHandler: Handler

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        mWorkerHandler = Handler(looper)
    }

    fun postTask(task: Runnable) {
        mWorkerHandler.post(task)
    }

    companion object {
        var INSTANCE: DbWorker? = null

        fun getInstance(): DbWorker? {
            if (INSTANCE == null) {
                synchronized(DbWorker::class) {
                    INSTANCE = DbWorker("DBWORKER")
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }
}
